angular.module('starter.services')

.factory('ApiRequest', function($rootScope, $http, $q, $ionicLoading, $ionicPopup, $cordovaFile, $cordovaFileTransfer){
	var ApiRequest = function(){
		var METHOD_GET = 'GET';
		var METHOD_POST = 'POST';
		
		var self = this;
		var baseUrl = $rootScope.BASE_HOST_API;
		var action = '';
		var method = METHOD_GET;
		var silent = false;
		var serverResponse = null;
		var fatalError = false;
		var debugmode = false;
		var production = false;
		var displayError = true;
		
		var deferred = $q.defer();
		var params = {};
		var targetFile = null;
		
		this.getErrors = function(){
			var errors = [];
			var curErrors;
			for( var key in serverResponse.errors ){
				curErrors = serverResponse.errors[key];
				
				if(typeof curErrors=='string'){
					errors.push(curErrors);
				}else{
					for( var i in curErrors ){
						errors.push(curErrors[i]);
					}
				}
			}
			
			return errors;
		};
		
		this.getResponse = function(){
			return serverResponse;
		};
		
		this.isFatalError = function(){
			return fatalError;
		};
		
		var addParam = function(key, value){
			if( value && typeof value!='undefined' ){
				var isArray = value ? (value.constructor == Array) : false;
				var paramvalue, paramname;
				if( !isArray ){
					params[key] = value;
				}else{
					for( var i in value ){
						paramvalue = value[i];
						paramname = key+'['+i+']';
						params[paramname] = paramvalue;
					}
				}
			}
			
			return this;
		};
		
		var setParams = function(object){
			params = object;
			return this;
		};
		
		var setFile = function(key, filePath){
			if( filePath )
				targetFile = {key: key, path:filePath};
		};
		
		var getAllParams = function(){
			return params;
		};
				
		var setGetMethod = function(){
			method = METHOD_GET;
		};
		
		var isGet = function(){
			return method == METHOD_GET;
		};
		
		var setPostMethod = function(){
			method = METHOD_POST;
		};
		
		var isPost = function(){
			return method == METHOD_POST;
		};
		
		var setAction = function(ac){
			action = ac;
		};
		
		var setSilent = function(sl){
			silent = sl;
		};
		
		var enableDebug = function(){
			debugmode = true;
		};
		
		var isSilent = function(){
			return silent;
		};
		
		var enableAutoError = function() {
			displayError = true;
		};
		
		var disableAutoError = function() {
			displayError = false;
		};
		
		var isAutoError = function() {
			return displayError;
		};
		
		var isDebug = function(){
			return debugmode && !production;
		};
		
		var reset = function(){
			params = [];
			method = METHOD_GET;
			silent = false;
			action = '';
		};
		
		var caller = function(func, params){
			if( typeof func == 'function' ){
				func.apply(this, params);
			}
		};
		
		var transformRequest = function(obj) {
			var qs = [];
			for(var key in obj){
				if( typeof obj[key] != 'undefined' )
					qs.push(encodeURIComponent(key) + "=" + encodeURIComponent(obj[key]));
			}
			
			return qs.join("&");
		}
		
		var transformResponse = function(resp){
			try {
				if( typeof resp=="string"){
					var response = JSON.parse(resp);
					return response;
				}else{
					return resp;
				}
			}
			catch(exception) {
				console.log('JSON.parse() error: '+exception);
				hideLoader();
				displayFatalError(resp);
				
				return false;
			}
		}
		
		var displayLoader = function(){
			$ionicLoading.show();
		};
		
		var hideLoader = function(){
			$ionicLoading.hide();
		};
		
		var getRequestUrl = function(){
			return baseUrl+action;
		};
		
		var request = function(){
			var promisse = null;
			if( !targetFile )
				promisse = defaultRequest();
			else
				promisse = fileUploadRequest();
			
			if( promisse ){
				promisse.finally(function(){
					if( !isSilent() )
						hideLoader();
				});
				return promisse;
			}
		}
		
		var defaultRequest = function(){
			var header = {};
			var paramsSend = '';
			var paramsFormated = '';
			var targetUrl = getRequestUrl();
			
			if( !isSilent() )
				displayLoader();
			
			paramsFormated = transformRequest(getAllParams());
			if( isPost() ){
				header['Content-Type'] = 'application/x-www-form-urlencoded';
				paramsSend = paramsFormated;
			}else{
				targetUrl += '?'+paramsFormated;
			}
			
			displayDebug(targetUrl);
			$http({
					method: method,
					url: targetUrl,
					headers: header,
					data: paramsSend,
					transformResponse: transformResponse,
				}).then(successCallback, failCallback);
			
			return deferred.promise;
		};
		
		var fileUploadRequest = function(){			
			setPostMethod();
			var paramsFormated = getAllParams();
			var targetUrl = getRequestUrl();
			var options = {
				fileKey:targetFile.key,
				//fileName: '',
				httpMethod: method,
				mimeType: 'image/jpeg',
				params: paramsFormated,
			};
			if( !isSilent() )
				displayLoader();
			
			displayDebug(targetUrl);
			$cordovaFileTransfer.upload(targetUrl, targetFile.path, options, false).then(successCallback, failCallback);
			
			return deferred.promise;
		}
		
		var successCallback = function(response){
			serverResponse = extractResponse(response);
			deferred.resolve(serverResponse);
		};
		
		var failCallback = function(response){
			if( response.body ){
				response.data = transformResponse(response.body);
			}
			
			serverResponse = extractResponse(response);
			if(response.data.errors){
				if( displayError ){
					$ionicPopup.alert({
						title: 'Erro',
						template: self.getErrors().join('<br>'),
					});
				}
			}else{
				displayFatalError(response);
			}
			
			deferred.reject(response);
		};
		
		var extractResponse = function(resp){
			var response = null;
			if( resp.response ){
				response = transformResponse(resp.response);
			}else{
				response = resp.data;
			}
			
			return response;
		};
		
		var displayFatalError = function(response){
			console.log('Fatal Error!');
			console.log(response);
			fatalError = true;
			alert('Hic Sunt Dracones');
		};
		
		var displayDebug = function(url){
			if( isDebug() ){
				console.log('['+method+'] '+url);
				console.log('Endpoint: '+action);
				console.log('Parameters');
				console.log(getAllParams());
				
				if(targetFile){
					if( targetFile.path.indexOf('base64')==-1){
						console.log('File path:['+targetFile.key+']'+targetFile.path );
					}else{
						console.log('File path:['+targetFile.key+']'+targetFile.path.substr(0, 50));
					}
				}
			}
		};
		
		return {			
			addParam: addParam,
			setFile: setFile,
			setParams: setParams,
			setGetMethod: setGetMethod,
			setPostMethod: setPostMethod,
			endpoint: setAction,
			setSilent: setSilent,
			enableAutoError: enableAutoError,
			disableAutoError: disableAutoError,
			isAutoError: isAutoError,
			request: request,
			debug: enableDebug,
			
			displayLoader: displayLoader,
			hideLoader: hideLoader,
		};
	};
	
	return ApiRequest;
});
